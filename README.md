# Survey Questions

## For SAP
1. What methods are you using to estimate mental health status of police?
	1. a written test? psychology activities
	2. face-to-face meeting
	3. Medical Device Evaluation
	
	
	
2.	Has SAP worked on or collaborated with any other company in order to carry out work on a similar project?


3.	What technology does the SAP prioritize to help AFP resolve this issue? 
	1. Artificial Intelligience 
	2. Mobile Application 
	3. Virtual Reality  
	4. others_____
5.	On a scale of 1-100, how confident do you feel that the SAP can come up with a framework that can be utilized by the AFP?
6.	On what lines/basis/technology/research does the SAP wants us(the students of ANU) to think upon in order to address this issue?
7.	What kind of method/system are you currently using to evaluate a possible case of mental illness?

8.	What other projects similar to this have SAP worked on previously?  Can you share any relevant research data or experience working on that project? Can we talk to the person who is running or was a part of that project?


9.	What sort of access to data or cooperation you can help us get from AFP?

 1. Trauma Records           
 2. Mental Situation Reports  
 3. Else:______

10.	Out of the following which technology do you think is most suitable to build the model of this this project. 

	1. Machine Learning        
	2. A.I        
	3. real-time data upload and analysis

	
	
11.	Do SAP has already planned or SAP has ever used software for similar project? SAP HANA / SAP HEC? What specific part of the project that the software is utilized for? Can we access the database?
12.	Are there any surveys or questionnaires available for us?
13.	Have you seen any policemen become better after change their roles? Are there any common characteristics?
14.	What level of security clearance can we get to system?




## For AFP

1.	Questions intended to AFP officers (mainly those who regularly work on the field):

	1.	Has AFP provided any specific training that is intended or proven to prevent or reduce risk of PTSD? Is this provided regularly?

2.	How long have you been working in AFP?
	1. 0-5 years 
	2. 5-10 years 
	3. 10-20 years 
	4. more than 20 years

3.	how do you feel when you are dealing with cases?
	1. very comfortable.
	2. comfortable
	3. normal
	4. uncomfortable
	5. very uncomfortable

4.	Are there any cases that will remind you and make you feel uncomfortable? Or have you experienced any unpleasant  events in your career?

	1. Yes, I have.
	2. No, I haven’t.
	3. I don’t remember.

5.	If the data is needed to be collected for your health evaluation, which way of mental health testing will makes you feel more comfortable?
	1. Paper testing 
	2. psychology exercise  
	3. face-to-face meeting  
	4. Medical Device Evaluation

6.	The frequency of mental tests you prefer is?
	1. Once a week 
	2. Once a month 
	3. Every six month 
	4. More than once a week 

	
7.	How many years have you served as a first respondant? 
8.	On a scale of 1-100, how helpful is the current mechanism that AFP has deployed to address the issue of PTSD?
9.	What in your opinion, can be done in order to improve this mechanism?
10.	On a scale of 1-100, how successful do you think are the rehabilitation programs for the officers who are trying to get back to work post psychological injury?
11.	Do you agree with the criticism that the AFP faced in the National Audit office Report regarding that the AFP does not currently have an effective mechanism to appropriately align resources with key mental health risks?
12.	Should AFP invest in a software company to help them resolve this issue?
13.	Do you think that mental health issues affect the performance on your employees. If yes, do you have any cases reported or any data that we can use?
14.	In what case do you think the mental issue would become trauma incidents?
15.	Has the AFP conducted any drill or activities that are used to help employees to deal with mental illness in the past few years? How are these going?
16.	What kind of help does you guys get from AFP organization on trauma?
17.	Do AFP has a specific department for your employees when they have been affected by some sort of trauma?  Are there any cases that wo could use?
18.	Are there any police officer who ever had trauma incidents, could you give us some details about them?        
19.	How many policemen that already have or potentially have mental health issues?
20.	What kinds of traumatic events that AFP’s employees normally facing? (Please survey at least 50 police officers if possible, or provide existing data from mental health department)
21.	Can you provide the ACT region reported crime location map (including type and number of crimes) for recent years?
22.	Can you provide the the reason and number of police officers resignation for recent years if possible?
23.	Have you seen any policemen become better after change their roles? Are there any common characteristics?










# Focus Group / Interview Questions

# For SAP




1. Has SAP made any initial investment or this project in particular? What form is it in? (in form of hardware, software, man-hours) 
	* how have these investment been utilized? 
	* For what specific purpose do these investments are being used in this project?
2. What expertise has SAP been allocating or dedicating for this project? 
		1. Software engineers 
		2. data analysts
		3. business analysts
		4. data scientists
		5. etc. 
	* what are their roles in this project? 
	* What specific tasks that they have been assigned to?



3. Has SAP planned or has been using SAP software (e.g SAP HANA S4 / SAP HEC) for this project? 
 - What specific part of the project that the software is utilized for? 
 - Are there any other third-party software or technology that has been used for this project either as a primary technology or as supporting technology to SAP’s technology? 
 - If this has not been done, is SAP open to having other organization(s) to partner with in this project?
4. What are the terms of information sharing that was agreed upon between SAP and AFP? 
	* Are there any certain information that SAP has been trying to get hands upon but are restricted by regulatory or contractual terms?

5. Has SAP considered or have been consulting an academic (or expert from other organization) that has done renowned researches relevant to this project? 
	* How did the advices from this expert affects this project?
	* Was there any technology-related advice form this expert?

	
6. According to our research, we found SAP has developed an advanced database management system - SAP HANA, which had been applied in a criminal related program in the U.S. It enables different sources of data (mental health history, criminal data,etc,.) interact with each other to predict that if an individual will return back to jail within 6 months. What we want to ask is :
	* are you applying SAP HANA in dealing with this issue? If not, what technology or database management system do you use for this problem now? 
	* How it works?
	* Is it a good solution to use a similar database to address this problem?

7. A formal report shows that the AFP’s information on employee mental health is held across a range of disconnected information systems and multiple paper documents which make it difficult for AFP to monitor and respond to emerging issues in employee mental health.
	* Is this true?
		* If it is, in which method you think is a good idea to help AFP to solve this problem?
			1. A new database system (electronic mental health records management)?
			* A new data collecting platform? 
			* or else

8. Do you already have a database which includes the mental health records? In the case you have, are these data collecting from a specific department or all AFP? In the case that you don't have this database, are there any ways to gather all the information?
9. Is there any electronic mental health records for the police officer? How does the AFP store their data? (The type of data which we can access to), what kind of limitations could we have
10. If we are trying to build a third party application, which kind of database can we use to set up the connection?
11. From the cases with  PTSD that are already identified ,is there a common denominator (a specific type of confronting that all of them had to deal with)?
12. The family issue or other factors apart from working may also affect the police officers’ mental health, do you have some methods to deal with these side effects ?
13. What Kind of data can we assume that SAP can provide us from AFP?
14. Will you provide us with any sort of existing data or do you need us to do  that research for you?
15. What is the current stage at which SAP is operating in terms of working on this issue? 
16. What do you expect from us as your potential collaborators on this project?
	* Are you looking for ideas, or are you looking for research data that supports the idea that AFP policemen are underperforming due to PTSD?

17. How long do you expect the output of this project shall be applicable to solve the current issues? 6-months or 12 months? or not beyond a certain period?
18. What kind of method/system are you currently using to evaluate a possible case of mental illness? Have you assigned any goals to improve the current tech?
19. From researches, we acknowledge that some other companies are trying to achieve the similar goals as you expected. 
	* Have you done any competitors analysis before? 
		* If yes, what’s the key factors do you care the most about from the analysis? If not, why?

20. Can we have the security clearances to access the data about the detail information of the police officers who tackle those traumatic events, including factors such as age, gender, marriage, region, role, employment time.(If SAP cannot provide this we will ask AFP)



	
# For AFP

1. Questions intended to AFP decision maker(s) in general (e.g. police chief):
	* How and why did AFP decided that PTSD is an important issue? 	* Was the decision made in declaring the mitigation of this problem financial or political or operational in nature? 
	* How was it initially identified and discovered? 
	* Was there any certain dataset that was used in declaring this issue?

2. Questions intended to AFP employee/officer that possesses the knowledge of AFP’s expenditures related to this mental health issue:
	* How much has the issue of PTSD risks among officers has costs AFP financially?
	* Has this issue of PTSD risk caused increased work insurance rate for AFP field officers?
	* Has this issue of PTSD risk caused early retirement among officers? 
	* Does this cost AFP initial investments in initial training of police officers?

3. Questions intended to AFP decision maker(s) related to the mental health issue:
	* Has AFP considered or had involved parties other than SAP in identifying causes of PTSD among officers? 
	* What may they have to offer that SAP cannot?
	* What are the main considerations of AFP choosing SAP as the main contractor to address this issue?
	* What does AFP expect from SAP that other organizations cannot offer?

4. Questions intended to AFP officers (mainly those who regularly work on the field):
	* Has AFP provided any specific training that is intended or proven to prevent or reduce risk of PTSD? 
		* Is this provided regularly?

5. Questions to AFP officers that control dispatches:
	*	What method has AFP been using to keep track of the location and condition of officers that are being dispatched in the field? 
		*	How and where is this data stored? 
		*	How thorough are these data? 
		*	What aspects of the dispatched officers are recorded?

6. Do all the police members have access to a smartphone?
7. Are there any policies or regulations in AFP that limits AFP employee’s access to smartphone during their work time?
	* What equipments do the police officers have? 
	* Is there any equipment can monitor the psycological data of police officer?
	* What is the frequency ratio of  cases regarding mental illness issues?

8.	AFP’s mental health services were criticised in National Audit office Report,Since then:
	* What are steps has the AFP taken to improve the current mechanism since then to counter the criticism mentioned in that report?
9. 	AFP commissioner Andrew Colvin stated that there has been a lot of investment which has been done over the years in employee health yet there is a long journey that they the AFP has to travel. 
	* What are these steps in that long journey, that AFP aims to cover?

10.	Are there any measures put up for the rehabilitation program for officers who are trying to get back to work after sustaining a psychological injury?
11. How often does screening processes take place to identify psychological readiness throughout the career of a police officer?
12. Taking Advice from phoenix australia was a great strategy, how far in your opinion did it help AFP to get a better idea of the situation? 
13. There was a debate going on regarding the psychological  disintegrations of your officers after they attend traumatic incidents during their work . 
	* Do your think, the current level of training your officers were trained on is better prepare them for traumatic event? 
	* When was the last time, an audit was done to the training methods? 

14.	What is the usual arrangement for serious cases like murder or other traumatic cases? 
	* Will these cases always be dealt with by a specific group of police? 
	* Or when will new police start tackling these cases?

15.	At present, is there any activities or methods for relieving mental pressure? 
	* How do participants or police involved reflect?

16.	Is there any regular communication between police officers and the organisation representatives? 
	* How is it going? 
	* And if possible, may I ask what the general topics are?

	
17.	May we have the security clearances to access the data about the detail information of the police officers who tackle those traumatic events, including factors such as age, gender, marriage, region, role, employment time.
18. What kinds of traumatic events that AFP’s employees normally facing? (To see whether the leaders of police have the same view as the employees)




















